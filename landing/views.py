from django.shortcuts import render, redirect
from .forms import FormStatus
from .models import PostStatus

# Create your views here.
def stat(request):
    postStatus = PostStatus.objects.all()
    formStatus = FormStatus(request.POST or None)
    if request.method == 'POST':
        if formStatus.is_valid():
            formStatus.save()
            return redirect('/')
    args = {
        'status' : postStatus,
        'form'	 : formStatus,
    }
    
    return render(request, 'status.html', args)
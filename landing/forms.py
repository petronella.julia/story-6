from django import forms
from .models import PostStatus
from django.forms import ModelForm


class FormStatus(forms.ModelForm):
    class Meta:
        model = PostStatus
        fields = ['status']
